#!/bin/Rscript

library(optparse)
sourcedir = ("/Users/hoeps/PhD/projects/huminvs/analyses_paper/packages/mendeltests/R/")
sourcefiles = list.files(sourcedir)
sapply(paste0(sourcedir,sourcefiles), source)
    
# INPUT
option_list = list(
make_option(c("-i", "--infile"), type="character", default=NULL,
            help="Genotype file the be mendeltested", metavar="character"),
make_option(c("-o", "--outfile"), type="character", default="./outputcorr/",
            help="Outputfile for that genotype file", metavar="character")
)
opt <- parse_args(OptionParser(option_list=option_list))

infile = opt$infile
outfile = opt$outfile
    
print(infile)
    
run_main(infile, outfile)
print('done')
